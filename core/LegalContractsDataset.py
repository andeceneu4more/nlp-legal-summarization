from torch.utils.data import Dataset
import json

class LegalContractsDataset(Dataset):
    def __init__(self, json_path):
        super().__init__()
        self.json_path = json_path
        with open(json_path, 'r') as fin:
            self.master_dict = json.load(fin)
        self.keys = list(self.master_dict.keys())

    def __len__(self):
        return len(self.keys)

    def __getitem__(self, idx):
        key = self.keys[idx]
        this_dict = self.master_dict[key]

        text = this_dict['original_text']
        summary = this_dict['reference_summary']
        return text, summary