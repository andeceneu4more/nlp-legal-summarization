from torch.utils.data import Dataset
import pandas as pd

from __init__ import pegasus_path
from transformers import PegasusTokenizer
import torch
import pdb

class ProcessedContractsDataset(Dataset):
    """ reads the input from dataframe and tokenizes it optaining also the attention mask and the positionals
    """
    def __init__(self, dataset_path, max_input_length):
        super().__init__()
        self.df = pd.read_csv(dataset_path, sep=',')
        self.max_input_length = max_input_length
        self.tokenizer = PegasusTokenizer.from_pretrained(pegasus_path, sep_token='</s>')

    def __len__(self):
        return len(self.df)

    def __getitem__(self, idx):
        df_line = self.df.iloc[idx]
        text = df_line['text']
        summary = df_line['summary']

        text_dict = self.tokenizer(text, padding=True, pad_to_multiple_of=self.max_input_length,
                                   truncation=True, max_length=self.max_input_length)
        text_enc = torch.tensor(text_dict['input_ids'])
        text_mask = torch.tensor(text_dict['attention_mask'])

        # text_positionals = list(map(int, df_line['text_positionals'].split(' ')))
        # text_positionals = torch.cat([
        #     torch.tensor(text_positionals, dtype=torch.int32),
        #     torch.zeros(self.max_input_length - len(text_positionals), dtype=torch.int32)
        # ])
        summ_dict = self.tokenizer(summary, padding=True, pad_to_multiple_of=self.max_input_length,
                                   truncation=True, max_length=self.max_input_length)
        summ_enc = torch.tensor(summ_dict['input_ids'])
        
        return text_enc, text_mask, summ_enc

    def get_pad_token_id(self):
        return self.tokenizer.pad_token_id