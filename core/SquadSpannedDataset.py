from torch.utils.data import Dataset
import pandas as pd

from __init__ import pegasus_path
from transformers import PegasusTokenizer
import torch
import pdb

class SquadSpannedDataset(Dataset):
    """ reads the input from dataframe and tokenizes it optaining also the attention mask
    """
    def __init__(self, dataset_path, max_input_length):
        super().__init__()
        self.df = pd.read_csv(dataset_path, sep=',')
        self.max_input_length = max_input_length
        self.tokenizer = PegasusTokenizer.from_pretrained(pegasus_path, sep_token='</s>')

    def __len__(self):
        return len(self.df)

    def __getitem__(self, idx):
        df_line = self.df.iloc[idx]
        context = df_line['context']
        question = df_line['question']
        start_pos = torch.tensor(df_line['start_span'])
        end_pos = torch.tensor(df_line['end_span'])

        both_dict = self.tokenizer(context, question, 
                                   padding=True, pad_to_multiple_of=self.max_input_length,
                                   truncation=True, max_length=self.max_input_length)
        both_enc = torch.tensor(both_dict['input_ids'])
        both_mask = torch.tensor(both_dict['attention_mask'])

        return both_enc, both_mask, start_pos, end_pos

    def get_pad_token_id(self):
        return self.tokenizer.pad_token_id