import torch.nn as nn
import torch.nn.functional as F

class SummarisationLoss(nn.Module):
    def __init__(self, pad_token_id):
        super().__init__()
        self.criterion = nn.CrossEntropyLoss()
        self.pad_token_id = pad_token_id
        
    def forward(self, output, target):
        x = F.log_softmax(output, dim=-1).squeeze(0)
        norm = (target != self.pad_token_id).data.sum()
        loss = self.criterion(x, target.squeeze(0)) / norm
        return loss