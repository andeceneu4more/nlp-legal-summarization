import os

legal_dataset_path = os.path.join('data', "all_v1.json")
legal_processed_path = os.path.join('data', "all_df.csv")
train_ds_path = os.path.join('data', "legal_train.csv")
test_ds_path = os.path.join('data', "legal_test.csv")

squad_path = os.path.join('data', 'squad', "extracted.json")
squad_ds_path = os.path.join('data', "all_qa.csv")
train_qa_path = os.path.join('data', "train_qa.csv")
test_qa_path = os.path.join('data', "test_qa.csv")

summ_folder = os.path.join('logs', 'pegasus-summarizer')
qa_folder = os.path.join('logs', 'bart_qa')

pegasus_path = 'google/pegasus-xsum'
t5_small_path = 't5-small'
t5_base_path = 't5-base'

