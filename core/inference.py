import argparse
from transformers import PegasusForConditionalGeneration
import torch
import pdb

from rouge import Rouge
import numpy as np
import matplotlib.pyplot as plt
import os


from __init__ import pegasus_path, train_ds_path, test_ds_path
from ProcessedContractsDataset import ProcessedContractsDataset, PegasusTokenizer

def get_most_relevant_gen(summary_enc, tokenizer):
    """ searches for the first generated string that is not void
    """
    i = 0
    decoded = tokenizer.decode(summary_enc[i], skip_special_tokens=True)
    while i < summary_enc.shape[0] and decoded == '':
        i += 1
        decoded = tokenizer.decode(summary_enc[i], skip_special_tokens=True)
    return summary_enc[i]

def generate_summary(model, data, device, config, tokenizer, rouge):
    """ generates the summary and computes the rouge loss if possible
    """
    text_enc, text_mask, summ_enc = data
    text_enc, text_mask, summ_enc = text_enc.unsqueeze(0).to(device), text_mask.unsqueeze(0).to(device), summ_enc.to(device)

    summary_enc = model.generate(
        text_enc,
        attention_mask=text_mask,
        max_length=summ_enc.shape[0], 
        num_beams=50,
        early_stopping=True,
        num_return_sequences=5
    )
    summary_g = get_most_relevant_gen(summary_enc)
    summary_generated = tokenizer.decode(summary_g, skip_special_tokens=True)
    summary_original = tokenizer.decode(summ_enc, skip_special_tokens=True)

    if summary_generated == '':
        return 0
    return rouge.get_scores(summary_generated, summary_original)




def main(config):
    max_input_length = config['max_input_length']
    device = torch.device('cuda') if not torch.cuda.is_available() else torch.device('cpu')
    torch.cuda.empty_cache()

    model = PegasusForConditionalGeneration.from_pretrained(config['model_path']).to(device)
    tokenizer = PegasusTokenizer.from_pretrained(pegasus_path)

    training_dataset = ProcessedContractsDataset(train_ds_path, max_input_length)
    testing_dataset = ProcessedContractsDataset(test_ds_path, max_input_length)

    rouge = Rouge()
    scores = []
    for i, data in enumerate(testing_dataset):
        score = generate_summary(model, data, device, config, tokenizer, rouge)
        scores.append(score)
        print(f'Inference progress {i}/{len(testing_dataset)}', end='\r')
    
    plt.figure(figsize=(15, 10))
    plt.hist(scores, bins=30)
    plt.savefig(os.path.join(config['model_path'], 'rouge_hist.png'))
    print(np.mean(scores))
        


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--max-inpu-length', dest='max_input_length', type=int, default=256, \
                        help='The maximum number of tokens inside a forward pass')
    parser.add_argument('--model-path', dest='model_path', type=str, default='.\\logs\\pegasus-summarizer\\1', \
                        help='The size of the training and testing batch from dataloader')
    parser.add_argument('--num-beams', dest='num_beams', type=int, default=8,
                        help='The lenght of the beam for the beam search process')
    args = parser.parse_args()
    args = vars(args)
    # transfer weights between the models 
    # train after transfer - direct or vice - versa
    # plot and document
    main(args)