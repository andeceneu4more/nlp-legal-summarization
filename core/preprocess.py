from __init__ import legal_dataset_path, pegasus_path, legal_processed_path, test_ds_path, train_ds_path
from LegalContractsDataset import LegalContractsDataset
from transformers import PegasusTokenizer

from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
import argparse
import pdb


def insert_list_at(input_, index_, list_):
    """ insert a list at a specified index
    """
    result = input_[:index_] + list_ + input_[index_ + 1:]
    return result

def get_end_index(word_index, words, max_input_length, tokenizer):
    """ returns a dynamic computed end index of the sequence in order that the tokenizer encoding will fit into the desired input
    """
    margin = 6
    word_end_index = np.min([word_index + max_input_length - margin, len(words)])
    sequence = tokenizer.encode(" ".join(words[word_index: word_end_index]))

    while len(sequence) >= max_input_length:
        margin += 2
        word_end_index = np.min([word_index + max_input_length - margin, len(words)])
        sequence = tokenizer.encode(" ".join(words[word_index: word_end_index]))
    return word_end_index

def get_splited_sents(input_sents, summ_sents, input_idx, max_input_length, tokenizer):
    """ splits a large sentence into multiple samples and repeat the summary sentence 
    """
    splitted_sents = []
    words = input_sents[input_idx].split(' ')
    word_index = 0
    n_repeated = 0
    while word_index < len(words):
        word_end_index = get_end_index(word_index, words, max_input_length, tokenizer)
        splitted_sents.append(" ".join(words[word_index: word_end_index]))
        word_index = word_end_index
        n_repeated += 1

    summ_index = input_idx % len(summ_sents)
    repeated_summ = summ_sents[summ_index] + tokenizer.sep_token

    input_sents = insert_list_at(input_sents, input_idx, splitted_sents)
    summ_sents = insert_list_at(summ_sents, summ_index, n_repeated * [repeated_summ])
    return input_sents, summ_sents

def get_segments(text, summary, max_input_length, tokenizer):
    """ computes the segments of (text, summary) that fit to the input shape it's made by wraping the text to the maximum output
    """
    text_list = []
    summ_list = []
    text_positionals = []

    input_sents = text.split('.')[:-1]
    summ_sents = summary.split('.')[:-1]
    input_idx = 0
    while input_idx < len(input_sents):
        batch_text = ''
        batch_summ = ''
        text_pos = []
        
        if len(tokenizer.encode(input_sents[input_idx])) >= max_input_length:
            input_sents, summ_sents = get_splited_sents(input_sents, summ_sents, input_idx, max_input_length, tokenizer)

        while len(tokenizer.encode(batch_text + input_sents[input_idx])) < max_input_length:
            batch_text += input_sents[input_idx] + f' {tokenizer.sep_token}'
            batch_summ += summ_sents[input_idx % len(summ_sents)] + f' {tokenizer.sep_token}'

            # tokenizer.train(input_sents[input_idx])
            # tokenizer.train([input_idx % len(summ_sents)])
            text_pos.append(input_idx)
            input_idx += 1
            if input_idx >= len(input_sents):
                break
        
        
        text_list.append(batch_text)
        summ_list.append(batch_summ)
        text_positionals.append(text_pos)
    # if there are sentences in the summary put them on the final element
    if len(input_sents) < len(summ_sents):
        summ_list[-1] += tokenizer.sep_token + tokenizer.sep_token.join(summ_sents[input_idx:])
        if len(tokenizer.encode(summ_list[-1])) >= max_input_length:
            print()
            print('summary len exceded\n')
    return text_list, summ_list, text_positionals


def get_segmentated_inputs(dataset, max_input_length, tokenizer):
    """ gets the segments of all input dataset
    """
    text_list = []
    summ_list = []
    text_pos_list = []
    for idx, (text, summary) in enumerate(dataset):
        text_segments, summ_segments, text_positionals = get_segments(text, summary, max_input_length, tokenizer)
        
        text_list += text_segments
        summ_list += summ_segments
        text_pos_list += text_positionals
        print(f"Separation progress {idx}/{len(dataset)}", end="\r")
    print()
    return text_list, summ_list, text_pos_list


def main(config):
    max_input_length = config['max_input_length']
    dataset = LegalContractsDataset(legal_dataset_path)
    tokenizer = PegasusTokenizer.from_pretrained(pegasus_path, sep_token='</s>', pad_token='<pad>',
                                                 padding=True, pad_to_multiple_of=max_input_length)
    text_list, summ_list, text_pos_list = get_segmentated_inputs(dataset, max_input_length, tokenizer)
    all_df = pd.DataFrame({
        'text': text_list,
        'summary': summ_list,
        'text_positionals': text_pos_list
    })
    all_df.to_csv(legal_processed_path, sep=',', index=None)
    
    X_train_indices, X_test_indices, _, _ = train_test_split(np.arange(len(all_df)), \
                                                             np.zeros(len(all_df)), \
                                                             test_size=0.2, shuffle=True, \
                                                             random_state=101)
    train_df = all_df.loc[X_train_indices]
    test_df = all_df.loc[X_test_indices]
    train_df.to_csv(train_ds_path, index=None)
    test_df.to_csv(test_ds_path, index=None)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--max-inpu-length', dest='max_input_length', type=int, default=256, \
                        help='The maximum number of tokens inside a forward pass')
    args = parser.parse_args()
    args = vars(args)
    main(args)