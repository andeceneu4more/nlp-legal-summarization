from __init__ import squad_path, pegasus_path, squad_ds_path, train_qa_path, test_qa_path

from transformers import PegasusTokenizer
from sklearn.model_selection import train_test_split
import argparse
import json
import pandas as pd
import numpy as np
import pdb

general_index = 0

def get_spanning(encoded_context, encoded_answer):
    """ returns the spanning indices of the answer in the tokenized text 
    """
    len_answer = len(encoded_answer)
    len_conext = len(encoded_context) - len_answer

    start_span = -1
    for i in range(len_conext):
        if encoded_context[i: i + len_answer] == encoded_answer:
            start_span = i
            break
    end_span = start_span + len_answer

    if start_span != -1:
        return start_span, end_span
    return None, None

def get_qas_df(qa_dict, tokenizer, context, encoded_context, question):
    """ collects the question answer needed parameters into a dataframe
    """
    this_df = pd.DataFrame()
    for answer_dict in qa_dict['answers']:
        answer_str = answer_dict['text']

        encoded_answer = tokenizer(answer_str)['input_ids'][:-1]
        start_span, end_span = get_spanning(encoded_context, encoded_answer)
        
        global general_index
        this_df = this_df.append(pd.Series({
            "context": context,
            "question": question,
            "start_span": start_span,
            "end_span": end_span
        }, name=general_index))
        
        general_index += 1
    return this_df

def get_df_from_paragraphs(paragraphs, tokenizer, max_input_length):
    """ computes a dataframe for each paragraph
    """
    all_qa = pd.DataFrame()
    for context_dict in paragraphs:
        context_dict = paragraphs[0]
        context = context_dict['context']
        
        encoded_context = tokenizer(context)['input_ids']
        assert len(encoded_context) < max_input_length, f'Context length exceeded in this tokenizer with {len(encoded_context)}'

        qas_dict = context_dict['qas']
        qa_dict = qas_dict[0]
        if not qa_dict['is_impossible']:
            question = qa_dict['question']
            this_df = get_qas_df(qa_dict, tokenizer, context, encoded_context, question)
            all_qa = all_qa.append(this_df)
    return all_qa

def main(config):
    max_input_length = config['max_input_length']
    tokenizer = PegasusTokenizer.from_pretrained(pegasus_path, sep_token='</s>', pad_token='<pad>')

    with open(squad_path, "r") as fin:
        squad_list = json.load(fin)

    all_qa = pd.DataFrame()
    for squad in squad_list:
        paragraphs = squad['paragraphs']
        paragraph_df = get_df_from_paragraphs(paragraphs, tokenizer, max_input_length)
        all_qa = all_qa.append(paragraph_df)
    
    all_qa['start_span'] = all_qa['start_span'].astype(int)
    all_qa['end_span'] = all_qa['end_span'].astype(int)
    all_qa.to_csv(squad_ds_path, index=None)

    X_train_indices, X_test_indices, _, _ = train_test_split(np.arange(len(all_qa)), \
                                                             np.zeros(len(all_qa)), \
                                                             test_size=0.1, shuffle=True, \
                                                             random_state=101)
    train_df = all_qa.loc[X_train_indices]
    test_df = all_qa.loc[X_test_indices]
    train_df.to_csv(train_qa_path, index=None)
    test_df.to_csv(test_qa_path, index=None)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--max-inpu-length', dest='max_input_length', type=int, default=256, \
                        help='The maximum number of tokens inside a forward pass')
    args = parser.parse_args()
    args = vars(args)
    main(args)