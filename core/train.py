from __init__ import pegasus_path, train_ds_path, test_ds_path, summ_folder
from ProcessedContractsDataset import ProcessedContractsDataset
from SummarisationLoss import SummarisationLoss

from transformers import PegasusForConditionalGeneration

import torch
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from torch.optim import Adam, 
from torch.optim.lr_scheduler import ReduceLROnPlateau

import numpy as np
import argparse
import os


# def align_shapes(generated_ids, summary_ids):
#     """ aligns the generated output with the input summary
#     """
#     generated_shape = generated_ids.shape[1]
#     summary_shape = summary_ids.shape[1]

#     if generated_shape != summary_shape:
#         if generated_shape < summary_shape:
#             padding_option = (0, summary_shape - generated_shape, 0, 0)
#             generated_ids = F.pad(generated_ids, pad=padding_option, mode='constant', value=0)
#         else:
#             padding_option = (0, generated_shape - summary_shape, 0, 0)
#             generated_ids = F.pad(generated_ids, pad=padding_option, mode='constant', value=0)

#     return generated_ids, summary_ids


# def find_lcs(input_a, input_b):
#     """ computes the longest common subsequence of 2 lists using Sequence Matcher 
#     """
#     sm = SequenceMatcher(None, input_a, input_b)
#     len_lcs = torch.sum(torch.tensor([block.size for block in sm.get_matching_blocks()]))
#     return len_lcs

# def torch_rouge(generated, summary):
#     """ compute the rouge using the lcs length 
#     """
#     overlapping_count = find_lcs(generated, summary)
#     precision = overlapping_count / len(generated)
#     recall = overlapping_count / len(summary)
#     f1_score = 2.0 * ((precision * recall) / (precision + recall + 1e-8))
#     return f1_score

# def loss_rouge(generateds, summary):
#     """ computes the mean of each generated summary with the grand truth one
#     """
#     rouges = torch.tensor([torch_rouge(generated, summary) for generated in generateds])
#     return torch.mean(rouges)

def get_lr(optim):
    for param_group in optim.param_groups:
        return param_group['lr']

def get_abstractive_loss(model, data, summ_loss, device):
    """ custom implementation - performes a forward pass and returns a loss for all the batch inputs
    also try: https://discuss.huggingface.co/t/train-bart-for-conditional-generation-e-g-summarization/1904
    """
    text_enc, text_mask, _, summ_enc = data
    text_enc, text_mask, summ_enc = text_enc.to(device), text_mask.to(device), summ_enc.to(device)
    output = model(text_enc, attention_mask=text_enc)
    loss = summ_loss(output.logits, summ_enc)
    return loss

def train_nn(model, dataloaders, optim, lr_scheduler, n_epochs, model_name, exp_dir,  get_loss, summ_loss, device):
    """ performs the training process of the pegasus
    """
    print_frequency = 1
    best_loss = 10
    writer = SummaryWriter(os.path.join(exp_dir, 'runs/'))
    model_name = os.path.join(model_name, exp_dir)

    for epoch in range(n_epochs):
        for phase in ['train', 'test']:
            epoch_losses = []
            if phase == 'train':
                model.train()
            else:
                model.eval()
            
            for i, data in enumerate(dataloaders[phase]):
                optim.zero_grad()
                with torch.set_grad_enabled(phase == 'train'):
                    loss = get_loss(model, data, summ_loss, device)
                    
                    if phase == 'train':
                        loss.backward()
                        optim.step()

                epoch_losses.append(loss.item())
                average_loss = np.mean(epoch_losses)
                learning_rate = get_lr(optim)

                if (i + 1) % print_frequency == 0:
                    print(f'{phase}ing epoch {epoch}, iter = {i+1}/{len(dataloaders[phase])} ' + \
                          f'loss = {loss}, average_loss = {average_loss}, learning_rate = {learning_rate}', end='\r')
    
            if phase == 'test' and average_loss < best_loss:
                best_loss = average_loss

                torch.save({
                        'model': model.state_dict(),
                        'optimizer': optim.state_dict(),
                        "lr_scheduler": lr_scheduler.state_dict()
                    }, model_name)
            print()

            if phase == 'train':
                writer.add_scalar('Train/Loss', average_loss, epoch)
                writer.flush()
            else:
                writer.add_scalar('Test/Loss', average_loss, epoch)
                writer.flush()

                try:
                    lr_scheduler.step()
                except:
                    lr_scheduler.step(average_loss)

def get_exp_dir(model_folder):
    """ search for a valid index for the experiment and creates a folder with this name
    """
    experiments = [d for d in os.listdir(model_folder) if os.path.isdir(os.path.join(model_folder, d))]
    experiments = set(map(int, experiments))
    if len(experiments) > 0:
        possible_experiments = set(range(1, max(experiments) + 2))
        experiment_id = min(possible_experiments - experiments)
    else:
        experiment_id = 1
    exp_dir = os.path.join(model_folder, str(experiment_id))
    os.makedirs(exp_dir, exist_ok=True)
    return exp_dir

def main(config):
    batch_size = config['batch_size']
    max_input_length = config['max_input_length']
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    torch.cuda.empty_cache()
    
    model = PegasusForConditionalGeneration.from_pretrained(pegasus_path).to(device)

    training_dataset = ProcessedContractsDataset(train_ds_path, max_input_length)
    testing_dataset = ProcessedContractsDataset(test_ds_path, max_input_length)

    training_dataloader = DataLoader(training_dataset, batch_size=batch_size)
    testing_dataloader = DataLoader(testing_dataset, batch_size=batch_size)
    dataloaders = {
        'train': training_dataloader,
        'test': testing_dataloader,
    }
    optim = Adam(model.parameters(), lr=0.001, betas=(0.9, 0.98), eps=1e-9)
    lr_scheduler = ReduceLROnPlateau(optim,
                                     factor = 0.1,
                                     patience = 3,
                                     mode = 'min')
    pad_token_id = training_dataset.get_pad_token_id()
    summ_loss = SummarisationLoss(pad_token_id).to(device)
    n_epochs = 1
    model_name = 'logs/model.pth'
    exp_dir = get_exp_dir(summ_folder)
    train_nn(model, dataloaders, optim, lr_scheduler, n_epochs, model_name, exp_dir, get_abstractive_loss, summ_loss, device)

    # add reduce LROnPlateau and log it




if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--max-inpu-length', dest='max_input_length', type=int, default=128, \
                        help='The maximum number of tokens inside a forward pass')
    parser.add_argument('--batch-size', dest='batch_size', type=int, default=1, \
                        help='The size of the training and testing batch from dataloader')
    args = parser.parse_args()
    args = vars(args)
    main(args)
