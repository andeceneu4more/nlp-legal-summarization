from train import train_nn, get_exp_dir
from SquadSpannedDataset import SquadSpannedDataset
from __init__ import pegasus_path, train_qa_path, test_qa_path, qa_folder

from transformers import BartForQuestionAnswering
from torch.utils.data import DataLoader
from torch.optim import Adam
from torch.optim.lr_scheduler import ReduceLROnPlateau

import torch.nn as nn
import argparse
import torch

def get_answering_loss(model, data, _, device):
    """ computes the input dict and gets the loss which is a cathegorical cross entropy
    """
    both_enc, both_mask, start_posionals, end_posionals = data                                                          
    input_dict = {
        'input_ids': both_enc.to(device),
        'attention_mask': both_mask.to(device),
        'start_positions': start_posionals.to(device), 
        'end_positions': end_posionals.to(device)
    }
    output = model(**input_dict)
    return output.loss

def main(config):
    batch_size = config['batch_size']
    max_input_length = config['max_input_length']
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    torch.cuda.empty_cache()
    
    model = BartForQuestionAnswering.from_pretrained(pegasus_path).to(device)
    """
    Some weights of the model checkpoint at google/pegasus-xsum were not used when initializing BartForQuestionAnswering: [
        'final_logits_bias', 'model.encoder.layer_norm.weight', 'model.encoder.layer_norm.bias', 
        'model.decoder.layer_norm.weight', 'model.decoder.layer_norm.bias', 'model.encoder.embed_positions.weight', 
        'model.encoder.layernorm_embedding.weight', 'model.decoder.layernorm_embedding.bias', 'qa_outputs.weight', 'qa_outputs.bias']
    """

    training_dataset = SquadSpannedDataset(train_qa_path, max_input_length)
    testing_dataset = SquadSpannedDataset(test_qa_path, max_input_length)

    training_dataloader = DataLoader(training_dataset, batch_size=batch_size)
    testing_dataloader = DataLoader(testing_dataset, batch_size=batch_size)
    dataloaders = {
        'train': training_dataloader,
        'test': testing_dataloader,
    }
    optim = Adam(model.parameters(), lr=0.001, betas=(0.9, 0.98), eps=1e-9)
    qa_loss = nn.CrossEntropyLoss().to(device)
    n_epochs = 1
    model_name = 'logs/model_qa.pth'
    exp_dir = get_exp_dir(qa_folder)
    lr_scheduler = ReduceLROnPlateau(optim,
                                    factor = 0.1,
                                    patience = 3,
                                    mode = 'min')
    train_nn(model, dataloaders, optim, lr_scheduler, n_epochs, model_name, exp_dir, get_answering_loss, qa_loss, device)
    # train it like this/local or in collab
    # transfer the common weights and make the proper experiments


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--max-inpu-length', dest='max_input_length', type=int, default=128, \
                        help='The maximum number of tokens inside a forward pass')
    parser.add_argument('--batch-size', dest='batch_size', type=int, default=1, \
                        help='The size of the training and testing batch from dataloader')
    args = parser.parse_args()
    args = vars(args)
    main(args)