https://snowballstem.org/algorithms/romanian/stemmer.html

https://docs.google.com/document/d/18sb7pvMtXojadXyB2H96edyHIA_C0eT4DFVpaMEwdw0/edit
https://docs.google.com/spreadsheets/d/17W0t36jSkKdObrSOrmJRSHwnK4YwuZPj6aXIgE2TBOI/edit#gid=1141718404

https://tatoeba.org/ron/
https://www.manythings.org/anki/

text normalization 
[https://stackoverflow.com/questions/517923/what-is-the-best-way-to-remove-accents-normalize-in-a-python-unicode-string/518232#518232]
http://www.unicode.org/reports/tr44/#GC_Values_Table
https://pythex.org/

Simple summarization
https://arxiv.org/abs/1905.08836

GPTs:
https://github.com/huggingface/transformers
http://jalammar.github.io/illustrated-gpt2/
https://medium.com/walmartglobaltech/the-journey-of-open-ai-gpt-models-32d95b7b7fb2
